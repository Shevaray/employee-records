import csv

departments = list()
departments = [{"department": "Technical"}, {"department": "Support"}]

for dept in departments:
    print("Working Department: ", dept['department'])
    dept['noOfWorkers'] = input("Enter Number of workers: ")
    dept['workers'] = list()
    for workers in range(int(dept['noOfWorkers'])):
        dept['workers'].append({"name": input("Name of Employee: "), "salary": int(input("Enter Salary: "))})

csvArr = list()
for item in departments:
    for emp in item['workers']:
        csvArr.append({'name': emp['name'], 'department': item['department'], 'salary': emp['salary']})

for counts, idz in enumerate(csvArr):
    idz['id'] = counts
print(csvArr)


def generate_csv(files):
    with open('employee_st.csv', 'w', newline='\n') as file:
        fieldnames = ['name', 'department', 'salary']
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(files)


def search_name(name, name_list):
    result_1 = list(filter(lambda dep: dep['name'].lower() == name.lower(), name_list))
    if not name:
        return "[-] employee name is required"
    else:
        if len(result_1) > 0:
            for e1 in result_1:
                print(e1)

            if len(result_1) == 1:
                for employee in csvArr:
                    if result_1[0] == employee:
                        emp_name = input("update name: ")
                        if emp_name == "":
                            employee['name'] = result_1[0]['name']
                        else:
                            employee['name'] = emp_name

                        emp_dpt = input("update department: ")
                        if emp_dpt == "":
                            employee['department'] = result_1[0]['department']
                        else:
                            employee['department'] = emp_dpt

                        emp_salary = input("update salary: ")
                        if not emp_salary:
                            employee['salary'] = result_1[0]['salary']
                        else:
                            employee['salary'] = emp_salary
                        return employee

            else:
                emp_id = int(input("enter id: "))
                result_2 = list(filter(lambda worker: worker['id'] == emp_id, result_1))

                for employee in csvArr:
                    if result_2[0] == employee:
                        print(employee)
                        emp_name = input("update name: ")
                        if emp_name == "":
                            employee['name'] = result_2[0]['name']
                        else:
                            employee['name'] = emp_name

                        emp_dpt = input("update department: ")
                        if emp_dpt == "":
                            employee['department'] = result_2[0]['department']
                        else:
                            employee['department'] = emp_dpt

                        emp_salary = input("update salary: ")
                        if not emp_salary:
                            employee['salary'] = result_2[0]['salary']
                        else:
                            employee['salary'] = emp_salary
                        return employee
        else:
            return "[-] employee not found"


def search_dept(depts, department):
    result_1 = list(filter(lambda depp: depp['department'].lower() == depts.lower(), department))

    if not depts:
        return "[-] department is required"
    else:
        if len(result_1) > 0:
            for e1 in result_1:
                print(e1)

            if len(result_1) == 1:
                for dep in csvArr:
                    if result_1[0] == dep:
                        emp_name = input("update name: ")
                        if emp_name == "":
                            dep['name'] = result_1[0]['name']
                        else:
                            dep['name'] = emp_name

                        emp_dpt = input("update department: ")
                        if emp_dpt == "":
                            dep['department'] = result_1[0]['department']
                        else:
                            dep['department'] = emp_dpt

                        emp_salary = input("update salary: ")
                        if not emp_salary:
                            dep['salary'] = result_1[0]['salary']
                        else:
                            dep['salary'] = emp_salary
                        return dep

            else:
                emp_id = int(input("enter id: "))
                result_2 = list(filter(lambda worker: worker['id'] == emp_id, result_1))

                for dep in csvArr:
                    if result_2[0] == dep:
                        print(dep)
                        emp_name = input("update name: ")
                        if emp_name == "":
                            dep['name'] = result_2[0]['name']
                        else:
                            dep['name'] = emp_name

                        emp_dpt = input("update department: ")
                        if emp_dpt == "":
                            dep['department'] = result_2[0]['department']
                        else:
                            dep['department'] = emp_dpt

                        emp_salary = input("update salary: ")
                        if not emp_salary:
                            dep['salary'] = result_2[0]['salary']
                        else:
                            dep['salary'] = emp_salary
                        return dep
        else:
            return "[-] department not found"


def search_opt(option):
    options = {"name": 1, "department": 2}

    if not option:
        return "[*] please select one option"
    else:
        if option == options["name"]:
            name_opt = search_name(input("Employee name: "), csvArr)
            return name_opt
        elif option == options["department"]:
            dept_opt = search_dept(input("Department: "), csvArr)
            return dept_opt
        else:
            return "[-] invalid option"


while True:
    print("search options: for[name] enter 1 & for[department] enter 2")
    result = search_opt(int(input("search by: ")))
    print(result)

# # generate_csv(csvArr)




